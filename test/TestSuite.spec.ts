import {Builder, WebDriver} from "selenium-webdriver";
import {assert, expect} from "chai";
import {MainPage} from "../pageobject/MainPage";
import {AdvancedSearchPage} from "../pageobject/AdvancedSearchPage";
import {ResultSearchPage} from "../pageobject/ResultSearchPage";
import {AuthPage} from "../pageobject/AuthPage";
import {ProfilePage} from "../pageobject/ProfilePage";
import {NamesAndRatings} from "../core/NoAuthPage";
import {FavoritesFilmPage} from "../pageobject/FavoritesFilmPage";

const urlMain: string = "https://www.kinopoisk.ru"
const urlAdvancedSearch: string = "https://www.kinopoisk.ru/s/";
const titleFilm: string = "Терминатор";
const rate: number = 5;
const titlePageExp: string = "Кинопоиск. Все фильмы планеты.";
const login: string ="tester1010101";
const filmName: string = "Матрица";

describe("Kinopoisk", (): void => {
    let driver: WebDriver;
    let mainPage: MainPage;
    let resultSearchPage: ResultSearchPage;
    let advancedSearchPage: AdvancedSearchPage;

    beforeEach(async (): Promise<void> => {
        driver = new Builder().forBrowser("chrome").build();
        mainPage = new MainPage(driver);
        advancedSearchPage = new AdvancedSearchPage(driver);
        resultSearchPage = new ResultSearchPage(driver);
    });

    afterEach(async (): Promise<void> => {
        await driver.close();
    });

    it(`should find the name of the ${titleFilm} and its rating`, async (): Promise<void> => {
        await advancedSearchPage.open(urlAdvancedSearch);
        await advancedSearchPage.searchMovie(titleFilm.substring(0, 4));
        let fiveResult:NamesAndRatings[] = await resultSearchPage.getFiveResult();
        let obj: NamesAndRatings = fiveResult[0];
        for(let el of fiveResult) {
            if(el.titleFilm === titleFilm) {
                obj = el;
                break;
            }
        }
        expect(titleFilm).to.include(obj.titleFilm, titleFilm)
        assert(obj.rate > rate, `Ожидалось, что оценка фильма ${titleFilm} больше ${rate}`);
    });

    it(`should check page title`, async (): Promise<void> => {
        await mainPage.open(urlMain);
        const titlePage: string = await mainPage.getTitle();
        expect(titlePageExp).to.include(titlePage, titlePageExp);
    });

    it(`user authorization check`, async (): Promise<void> => {
        await mainPage.open(urlMain);
        const authPage: AuthPage = await mainPage.getAuth();
        await authPage.auth();
        const profile: ProfilePage = await authPage.clickLogin();
        const strLogin: string = await profile.getProfileName();
        expect(login).to.include(strLogin, `Ожидается ${login}, а получен ${strLogin}`);
    });

    it(`must search for the movie and include it in favorites`, async (): Promise<void> => {
        await mainPage.open(urlMain);
        const authPage: AuthPage = await mainPage.getAuth();
        await authPage.auth();
        const favoritesFilmPage: FavoritesFilmPage = await authPage.searchFilm(filmName);
        const filmNameFromPage: string = await favoritesFilmPage.getNameFilm();
        await favoritesFilmPage.removeFilmFavorite();
        expect(filmName).to.include(filmNameFromPage, `Ожидается в избранном ${filmName}, а получен ${filmNameFromPage}`);
    });
});