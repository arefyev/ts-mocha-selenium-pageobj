import {BasePage} from "../core/BasePage";
import {By, WebDriver} from "selenium-webdriver";

export class ProfilePage extends BasePage {
    private profileName: By = By.xpath("//div[@itemprop = 'name']");
    constructor(webdriver: WebDriver) {
        super(webdriver);
    }

    public async getProfileName(): Promise<string> {
        const str: string =  await this.driver.findElement(this.profileName).getText();
        return str.replace(new RegExp("[@].*"), "");
    }
}