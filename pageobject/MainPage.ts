import {BasePage} from "../core/BasePage";
import {By, WebDriver} from "selenium-webdriver";
import {AuthPage} from "./AuthPage";

export class MainPage extends BasePage {
    private advancedFilter: By = By.xpath("//a[@href='/s/']");
    private title: By = By.xpath("//title[text() = 'Кинопоиск. Все фильмы планеты.']");
    private buttonEnter: By = By.xpath("//button[text()='Войти']");

    constructor(webdriver: WebDriver) {
        super(webdriver);
    }

    public async clickAdvancedFilter(): Promise<void> {
        await this.driver.findElement(this.advancedFilter).click();
    }

    public async getTitle(): Promise<string> {
        return await this.driver.findElement(this.title).getText();
    }

    public async getAuth():Promise<AuthPage> {
        await this.driver.findElement(this.buttonEnter).click();
        return new AuthPage(this.driver);
    }
}