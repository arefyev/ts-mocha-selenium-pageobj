import {BasePage} from "../core/BasePage";
import {By, WebDriver, WebElement} from "selenium-webdriver";
import {NamesAndRatings} from "../core/NoAuthPage";

export class ResultSearchPage extends BasePage {
    private fiveResult: WebElement[] = [];
    private advancedSearchResult: By = By.xpath("//div[@class='element'] | //div[@class='element most_wanted']");
    private title: By = By.css('.name a');
    private rating: By = By.css('.rating');

    constructor(webdriver: WebDriver) {
        super(webdriver);
    }

    public async getFiveResult(): Promise<NamesAndRatings[]> {
        this.fiveResult = await this.driver.findElements(this.advancedSearchResult);
        const namesAndRatings: NamesAndRatings[] = [];
        for (const el of this.fiveResult) {
            const element: WebElement = el;
            const titles : string = await element.findElement(this.title).getText();
            const ratings : number = Number(await element.findElement(this.rating).getText());
            const nameAndRating : NamesAndRatings = {
                titleFilm: titles,
                rate: ratings
            }
            namesAndRatings.push(nameAndRating)
        }
        return namesAndRatings;
    }
}

