import {BasePage} from "../core/BasePage";
import {By, WebDriver} from "selenium-webdriver";

export class AdvancedSearchPage extends BasePage {
    private inputFindFilm: By = By.id("find_film");
    private listGenre: By = By.xpath("//select[@id= 'm_act[genre]']/option[@value = '3']");
    private buttonSearch: By = By.xpath("(//input[@type= 'button'])[1]");

    constructor(webdriver: WebDriver) {
        super(webdriver);
    }

    public async searchMovie(movieTitle: string): Promise<void> {
        await this.driver.findElement(this.inputFindFilm).sendKeys(movieTitle);
        await this.driver.findElement(this.listGenre).click();
        await this.driver.findElement(this.buttonSearch).click();
    }
}