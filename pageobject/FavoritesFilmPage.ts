import {BasePage} from "../core/BasePage";
import {By, until} from "selenium-webdriver";
import {FilmPage} from "./FilmPage";
import {AuthPage} from "./AuthPage";

export class FavoritesFilmPage extends BasePage {
    private nameFilm: By = By.xpath("//a[@class='name']");

    public async getNameFilm(): Promise<string> {
        return await this.driver.findElement(this.nameFilm).getText();
    }

    public async removeFilmFavorite(): Promise<void> {
        await this.driver.findElement(this.nameFilm).click();
        const filmPage: FilmPage = await new FilmPage(this.driver);
        await filmPage.addFavorites();
        const remove: AuthPage = await new AuthPage(this.driver);
        await remove.clickFilm();
    }
}

