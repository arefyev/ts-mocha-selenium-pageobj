import {BasePage} from "../core/BasePage";
import {By, Key, until, WebDriver} from "selenium-webdriver";
import {ProfilePage} from "./ProfilePage";
import {FilmPage} from "./FilmPage";
import {FavoritesFilmPage} from "./FavoritesFilmPage";


export class AuthPage extends BasePage {
    private email: By = By.id("passp-field-login");
    private passwordPage: By = By.id("passp-field-passwd");
    private buttonEnterAuth: By = By.id("passp:sign-in");
    private checkLogin: By = By.xpath("//div/div/button/div/div/parent::div/parent::button");
    private checkProfile: By = By.xpath("//a[contains(@href, '/user/')][1]");
    private searchString: By = By.xpath("//form/div/input[@name='kp_query']");
    private resultSearch: By = By.xpath("//div[@class='element most_wanted']/div/p/a");
    private linkMovie: By = By.xpath("//ul/li/a[@href='/mykp/movies/']");

    constructor(webdriver: WebDriver) {
        super(webdriver);
    }

    public async auth(): Promise<void>{
        await this.driver.findElement(this.email).sendKeys(this.login);
        await this.driver.findElement(this.buttonEnterAuth).click();
        await this.driver.wait(until.elementLocated(this.passwordPage), 10000).sendKeys(this.password);
        await this.driver.findElement(this.buttonEnterAuth).click();
    }

    public async clickLogin(): Promise<ProfilePage> {
        await this.driver.wait(until.elementLocated(this.checkLogin), 5000).click();
        await this.driver.findElement(this.checkProfile).click();
        return new ProfilePage(this.driver);
    }

    public async clickFilm(): Promise<void> {
        await this.driver.wait(until.elementLocated(this.checkLogin), 5000).click();
        await this.driver.findElement(this.linkMovie).click();
    }

    public async searchFilm(filmName: string): Promise<FavoritesFilmPage> {
        await this.driver.wait(until.elementLocated(this.searchString), 5000).sendKeys(filmName, Key.ENTER);
        await this.driver.findElement(this.resultSearch).click();
        const filmPage: FilmPage = await new FilmPage(this.driver);
        await filmPage.addFavorites();
        await this.clickFilm();
        return new FavoritesFilmPage(this.driver);
    }
}
