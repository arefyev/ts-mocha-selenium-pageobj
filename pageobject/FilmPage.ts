import {BasePage} from "../core/BasePage";
import {By, WebDriver} from "selenium-webdriver";

export class FilmPage extends BasePage {
    private buttonFavorites: By = By.xpath("//button[@title = 'Буду смотреть']");

    constructor(webdriver: WebDriver) {
        super(webdriver);
    }

    public  async addFavorites(): Promise<void> {
        await this.driver.findElement(this.buttonFavorites).click();
    }
}