import {WebDriver} from "selenium-webdriver";

interface Auth {
    login: string,
    password: string
}

export class BasePage implements Auth{
    protected driver: WebDriver;

    get login(): string {
        return "tester1010101@yandex.ru";
    }

    get password(): string {
        return "Tester0101";
    }

    public constructor(webdriver: WebDriver) {
        this.driver = webdriver;
        this.driver.manage().window().maximize();
    }

    public async open(url: string): Promise<void> {
        await this.driver.get(url);
    }

    public async close(): Promise<void> {
        await this.driver.quit();
    }
}

